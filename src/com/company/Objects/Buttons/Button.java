package com.company.Objects.Buttons;

import com.company.Objects.interfaces.Drawable;
import com.company.Util.Rect;

public abstract class Button implements Drawable {

    public abstract void draw();

    public abstract float getX();

    public abstract float getY();

}
