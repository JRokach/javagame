package com.company.Util;

import static org.lwjgl.opengl.GL11.*;

public class Rect {
    private float x;
    private float y;
    private float width;
    private float height;

    public Rect(float x, float y, float width, float height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public void draw() {
        glBegin(GL_QUADS);
        glVertex2f(getX() - getWidth(), getY() + getHeight()); // top left
        glVertex2f(getX() + getWidth(), getY() + getHeight()); // top right
        glVertex2f(getX() + getWidth(), getY() - getHeight()); // bottom right
        glVertex2f(getX() - getWidth(), getY() - getHeight()); // bottom left
        glEnd();
    }
}
