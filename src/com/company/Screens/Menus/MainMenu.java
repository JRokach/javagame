package com.company.Screens.Menus;

import com.company.Objects.Buttons.Button;
import com.company.Objects.Buttons.RectButton;
import com.company.Screens.Screen;
import org.lwjgl.opengl.GL;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.opengl.GL11C.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11C.glClear;

public class MainMenu extends Screen {

    protected void loop() {
        GL.createCapabilities();

        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        while (!glfwWindowShouldClose(window)) {
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer
            drawObjs();
            glfwSwapBuffers(window); // swap the color buffers
            glfwPollEvents();
        }
    }

    protected void initWinSize() {
        this.WIDTH = 1900;
        this.HEIGHT = 1080;
    }

    protected void init() {
        Button b = new RectButton(0.1f, 0.8f, 0.1f, 0.08f, "Start");
        Button b1 = new RectButton(0.2f, 0.1f, 0.2f, 0.1f, "Start");
        drawables.add(b);s
        drawables.add(b1);
    }
}
