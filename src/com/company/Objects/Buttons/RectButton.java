package com.company.Objects.Buttons;

import com.company.Util.Rect;

public class RectButton extends Button {

    private Rect rect;
    private String text;

    public RectButton(float x, float y, float width, float height, String text) {
        this.rect = new Rect(x, y, width, height);
        this.text = text;
    }

    public void draw() {
        rect.draw();
    }


    public float getX() {
        return rect.getX();
    }

    public float getY() {
        return rect.getY();
    }

    public float getWidth() {
        return rect.getWidth();
    }

    public float getHeight() {
        return rect.getHeight();
    }
}
