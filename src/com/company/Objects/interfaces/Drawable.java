package com.company.Objects.interfaces;

public interface Drawable {
    public void draw();
}
